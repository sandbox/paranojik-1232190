<?php

/**
 * @file
 * Hook implementations, callbacks form OpenID SSO.
 */

/**
 * Implements hook_menu().
 */
function openid_sso_relying_menu() {
  $items = array();
  // Move log in page to login/direct. See how openid_sso_relying_menu_alter() replaces
  // log in.
  $items['login/direct'] = array(
    'title' => 'Direct log in',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('user_login'),
    'access callback' => 'user_is_anonymous',
    'type' => MENU_CALLBACK,
  );
  $items['sso/init'] = array(
    'page callback' => 'openid_sso_relying_init_page',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['sso/logout-finalize'] = array(
    'page callback' => 'openid_sso_relying_logout',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['admin/config/people/openid-sso-relying'] = array(
    'title' => 'OpenID Simple Sign-On',
    'description' => 'Configure a designated OpenID provider for simplified OpenID-based sign-on.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('openid_sso_relying_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements hook_menu_alter().
 */
function openid_sso_relying_menu_alter(&$items) {
  $items['user']['page callback'] = 'openid_sso_relying_user_page';
  $items['user/register']['page callback'] = 'openid_sso_relying_user_register_page';
  $items['user/password']['page callback'] = 'openid_sso_relying_user_password_page';
  $items['user/logout']['page callback'] = 'openid_sso_relying_logout_page';
}

/**
 * Implements hook_form_alter().
 *
 * Redirect user to front page after login, otherwise she will be pushed to OP
 * when using the login/direct form.
 */
function openid_sso_relying_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    // Redirect user to front page after login, otherwise she will be pushed to
    // OP when using the login/direct form.
    case 'user_login':
      $form_state['redirect'] = '';
      break;
      // Don't allow the user to login using the login block. Direct her to OP
      // instead.
    case 'user_login_block':
      // Show a modal message when user clicks on log in (there may be a wait).
      $path = drupal_get_path('module', 'openid_sso_relying');
      drupal_add_js(array('openid_sso_relying_wait_message' => t('Please wait...')), array('type' => 'setting', 'scope' => JS_DEFAULT));
      drupal_add_css("$path/openid_sso_relying.css");
      drupal_add_js("$path/jquery.blockUI.js");
      drupal_add_js("$path/openid_sso_relying.js");

      // Remove all child elements.
      foreach (element_children($form) as $key) {
        unset($form[$key]);
      }
      $form['#action'] = url('user');
      $form['#validate'] = array();

      if ($provider = variable_get('openid_sso_relying_provider', array())) {
        $form['message'] = array(
          '#markup' => t('Login using @provider_name as your OpenID provider.', array('@provider_name' => $provider['name'])),
        );
        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Login'),
          '#submit' => array('openid_sso_relying_user_login_submit'),
          '#attributes' => array('class' => array('login-submit')),
        );
      }
      break;
  }
}

/**
 * Page callback for initiating an authentication request. Logs out user before
 * initiation.
 *
 * This page is requested rather than the user/ page because it makes sure that
 * a user is NOT authenticated when initiating the OpenID authentication
 * request.
 */
function openid_sso_relying_init_page() {
  openid_sso_relying_logout();
  drupal_goto('user');
}

/**
 * Custom menu callback for user/page.
 */
function openid_sso_relying_user_page() {
  global $user;
  if ($user->uid) {
    return user_page();
  }
  return openid_sso_relying_request();
}

/**
 * Custom menu callback for user/register.
 */
function openid_sso_relying_user_register_page() {
  $provider = variable_get('openid_sso_relying_provider', array());
  drupal_goto(url($provider['url'] . 'user/register', array('absolute' => TRUE)));
}

/**
 * Custom menu callback for user/password.
 */
function openid_sso_relying_user_password_page() {
  $provider = variable_get('openid_sso_relying_provider', array());
  drupal_goto(url($provider['url'] . 'user/password', array('absolute' => TRUE)));
}

/**
 * Custom logout callback, redirects to hub.
 */
function openid_sso_relying_logout_page() {
  global $user;
  // Only redirect external users to provider page
  if (db_select('authmap', 'a')->fields('a', array('uid'))->condition('uid', $user->uid)->execute()->fetchField()) {
    $provider = variable_get('openid_sso_relying_provider', array());
    drupal_goto($provider['url'] . 'sso/logout/', array('query' => array('realm' => url(NULL, array('absolute' => TRUE)),
                                                                         'logout_redirect' => url('sso/logout-finalize', array('absolute' => TRUE)))));
  }
  else {
    // User core logout process.
    user_logout();
  }
}

/**
 * Settings form.
 */
function openid_sso_relying_settings($form, &$form_state) {
  $provider = variable_get('openid_sso_relying_provider', array());
  $form = array();
  $form['openid_sso_relying_provider'] = array(
    '#type' => 'fieldset',
    '#title' => t('OpenID Provider'),
    '#description' => t('A designated OpenID Provider with Simple Sign-On support. This must be another Drupal site with OpenID Provider module and OpenID Provider SSO module installed and configured.'),
    '#tree' => TRUE,
  );
  $form['openid_sso_relying_provider']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('The site name of the provider.'),
    '#default_value' => isset($provider['name']) ? $provider['name'] : '',
  );
  $form['openid_sso_relying_provider']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('The full URL of the provider, must contain a trailing slash.'),
    '#default_value' => isset($provider['url']) ? $provider['url'] : '',
  );

  $user_fields = array(); // used for mapping_name => label value
  // Define general profile fields
  $user_fields = array(
    'name' => t('Username'),
    'mail' => t('Email'),
    'picture' => t('Picture'),
    'timezone' => t('Timezone'),
    'language' => t('Language'),
  );
  // Define additional user profile fileds
  $instances = field_read_instances(array('entity_type' => 'user', 'bundle' => 'user'));
  foreach ($instances as $field_instance) {
    $field_name = $field_instance['field_name'];
    $field_label = $field_instance['label'];
    $user_fields[$field_name] = $field_label;
  }

  // Define read-only fields on the client
  $form['readonly'] = array(
    '#type' => 'fieldset',
    '#title' => t('Define read-only user profile fields.'),
    '#description' => t('Select the fields that can only be changed on the provider side.'),
  );
  $form['readonly']['openid_sso_relying_read_only_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Readonly fields.'),
    '#options' => $user_fields,
    '#default_value' => variable_get('openid_sso_relying_read_only_fields', array()),
  );

  return system_settings_form($form);
}

/**
 * Guarantee a trailing slash.
 */
function openid_sso_relying_settings_validate($form, &$form_state) {
  $form_state['values']['openid_sso_relying_provider']['url'] = trim($form_state['values']['openid_sso_relying_provider']['url'], '/') . '/';
}

/**
 * Request authentication.
 */
function openid_sso_relying_request() {
  $provider = variable_get('openid_sso_relying_provider', array());
  $front = drupal_get_normal_path(variable_get('site_frontpage', 'node'));
  $values = array(
    'openid_identifier' => $provider['url'],
    'openid.return_to' => url('openid/authenticate', array('absolute' => TRUE, 'query' => array('destination' => $front))),
  );
  openid_begin($values['openid_identifier'], $values['openid.return_to'], $values);
}

/**
 * Pillaged from user_logout(). Does not contain drupal_goto().
 */
function openid_sso_relying_logout() {
  global $user;

  watchdog('user', 'Session closed for %name.', array('%name' => $user->name));

  module_invoke_all('user_logout', $user);

  // Destroy the current session, and reset $user to the anonymous user.
  session_destroy();
}

function openid_sso_relying_user_external_name($uid) {
  return db_select('authmap', 'a')
          ->fields('a', array('authname'))
          ->condition('uid', $uid)
          ->condition('module', 'openid')
          ->execute()
          ->fetchField();
}

/**
 * Implement HOOK_form_FORM_ID_alter().
 */
function openid_sso_relying_form_user_profile_form_alter(&$form, &$form_state) {
  $account = user_load($form['#user']->uid);

  // Don't alter the form for internal users
  if ($authname = openid_sso_relying_user_external_name($account->uid)) {
    // Get the external user id, we know this, because we are connecting to a sso OP
    if (preg_match('/user\/([0-9]+)\/identity$/', $authname, $matches)) {
      $aid = $matches[1];

      $provider = variable_get('openid_sso_relying_provider', array());

      $path = isset($_GET['q']) ? $_GET['q'] : '';
      $path = url($path, array('absolute' => TRUE));

      // Message for readonly fields contains edit url on provider side,
      // and redirect url back to RP
      $description = t('Please visit the !sso_name to change this field.',
                       array('!sso_name' => l(t('Single-Sign-On-Provider'),
                                              $provider['url'] . 'user/' . $aid . '/edit',
                                              array('query' => array('destination' => $path)))));
    }
    else {
      $description = t('This field is readonly.');
    }
    $read_only = variable_get('openid_sso_relying_read_only_fields', array());
    
    // Recursively traverse the form and disable selected fields
    _openid_sso_relying_disable_user_fields($form, $read_only, $description);

    // Password field is allways disabled
    $form['account']['current_pass']['#access'] = FALSE;
    $form['account']['pass']['#access'] = FALSE;
    $form['account']['pass']['#disabled'] = TRUE;
    $form['account']['pass']['#type'] = 'textfield';
    $form['account']['pass']['#title'] = t('Password');
    $form['account']['pass']['#description'] = $description;
  }
}

/**
 * Recursively traverses the form and disables form elements that are marked
 * int the $read_only array.
 *
 * @param type $element - The form.
 * @param type $read_only - Elements to disable.
 * @param type $description - Description that is appended to the disabled fields.
 */
function _openid_sso_relying_disable_user_fields(&$element, $read_only, $description) {
  foreach (element_children($element) as $key) {
    if (array_key_exists($key, $read_only) && $read_only[$key]) {
      $element[$key]['#disabled'] = TRUE;
      $element[$key]['#description'] = $description;
    }
    _openid_sso_relying_disable_user_fields($element[$key], $read_only, $description);
  }
}

